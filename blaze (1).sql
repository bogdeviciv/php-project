-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2019 at 11:03 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blaze`
--

-- --------------------------------------------------------

--
-- Table structure for table `blazeaccounts`
--

CREATE TABLE `blazeaccounts` (
  `BLUserID` int(11) NOT NULL,
  `BLFaceBID` varchar(20) NOT NULL,
  `BLFirstName` varchar(45) NOT NULL,
  `BLAge` smallint(6) DEFAULT NULL,
  `BLHeight` decimal(10,0) DEFAULT NULL,
  `BLInterests` varchar(256) DEFAULT NULL,
  `BLGender` enum('Male','Female','Other') DEFAULT 'Other',
  `BLAboutMe` varchar(512) DEFAULT NULL,
  `BLWork` varchar(45) DEFAULT NULL,
  `BLAgeRange` int(50) DEFAULT NULL,
  `BLLocation` varchar(50) DEFAULT NULL,
  `BLPhoto` varchar(50) DEFAULT NULL,
  `BLEducation` varchar(100) DEFAULT NULL,
  `BLEthnicity` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `ConvoID` int(11) NOT NULL,
  `CTimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CAuthorID` int(11) NOT NULL,
  `CReceiver` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `LikeID` int(11) NOT NULL,
  `BLUserID` int(11) NOT NULL,
  `LLikedUserID` int(11) NOT NULL,
  `ConvoID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `MessageID` int(11) NOT NULL,
  `ConvoID` int(11) NOT NULL,
  `MsgContent` varchar(450) NOT NULL,
  `MsgTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reportedusers`
--

CREATE TABLE `reportedusers` (
  `ReportID` int(11) NOT NULL,
  `BLUserID` int(11) NOT NULL,
  `BLReportedUserID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blazeaccounts`
--
ALTER TABLE `blazeaccounts`
  ADD PRIMARY KEY (`BLUserID`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`ConvoID`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`LikeID`),
  ADD KEY `BLUserID` (`BLUserID`),
  ADD KEY `ConvoID` (`ConvoID`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`MessageID`),
  ADD KEY `ConvoID` (`ConvoID`);

--
-- Indexes for table `reportedusers`
--
ALTER TABLE `reportedusers`
  ADD PRIMARY KEY (`ReportID`),
  ADD KEY `BLUserID` (`BLUserID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blazeaccounts`
--
ALTER TABLE `blazeaccounts`
  MODIFY `BLUserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `ConvoID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `LikeID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `MessageID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reportedusers`
--
ALTER TABLE `reportedusers`
  MODIFY `ReportID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`BLUserID`) REFERENCES `blazeaccounts` (`BLUserID`),
  ADD CONSTRAINT `likes_ibfk_2` FOREIGN KEY (`ConvoID`) REFERENCES `conversations` (`ConvoID`);

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`ConvoID`) REFERENCES `conversations` (`ConvoID`);

--
-- Constraints for table `reportedusers`
--
ALTER TABLE `reportedusers`
  ADD CONSTRAINT `reportedusers_ibfk_1` FOREIGN KEY (`BLUserID`) REFERENCES `blazeaccounts` (`BLUserID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
