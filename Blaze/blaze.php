<?php

session_start();
require_once 'vendor/autoload.php';

// MONOLOG USAGE
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));
///////////////////////////////////////


if (false) {
    DB::$user = 'blaze';
    DB::$dbName = 'blaze';
    DB::$password = 'dOPTUGD6DUbgtf10';
    DB::$port = 3306; //Working from home use this port
    //DB::$port = 3333;
    DB::$host = 'localhost';
    DB::$encoding = 'utf8';
} else {


    DB::$user = "cp4907_blaze";
    DB::$dbName = "cp4907_blaze";
    DB::$password = "ipd15";
    DB::$encoding = 'utf8';
}

DB::$error_handler = 'db_error_handler';

//function db_error_handler($params) {
//    global $app, $log;
//    $log->error("Error: " . $params['error']);
//    $log->error("Query: " . $params['query']);
//    http_response_code(500);
//    $app->render('fatal_error.html.twig');
//    die;
//}
// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');



// VICTORIA'S TERRITORY ***********************************
$app->get('/', function() use ($app) {

    $app->render('main.html.twig');
})->name('main');

$app->get('/loginsuccess/:id/:name', function($fb_id, $name) use ($app) {

    $_SESSION['name'] = $name;
    $_SESSION['fb_id'] = $fb_id;
    $user = DB::queryFirstRow("SELECT * from blazeaccounts WHERE BLFaceBID=%s", $fb_id);
    if (!$user) {
        //insert
        DB::insert('blazeaccounts', array(
            'BLFirstName' => $name,
            'BLFaceBID' => $fb_id,
        ));
        $id = DB::insertId();
        $_SESSION['id'] = $id;
    } else {
        $_SESSION['id'] = $user['BLUserID'];
        $app->response->redirect($app->urlFor('home'), 303);
        return;
    }
    $app->response->redirect($app->urlFor('home'), 303);
});

$app->get('/profileedit', function() use ($app) {
//first show
    $id = $_SESSION['id'];
    $current_user = DB::queryFirstRow("SELECT * FROM blazeaccounts WHERE BLUserId=%s", $id);
    //print_r($current_user);



    $app->render("profileedit.html.twig", array('u' => $current_user));
});

$app->post('/profileedit', function() use ($app) {
//first show
    if(!isset($_SESSION['id'])){
        $app->render("unauthorised.html.twig");
        return;
    }
    $id=$_SESSION['id'];
    $errorList = array();

    $about = $app->request()->post('about');
    $lookingfor = $app->request()->post('lookingfor');
    $ageFrom = $app->request()->post('ageFrom');
    $ageTo = $app->request()->post('ageTo');

    $education = $app->request()->post('education');
    $interests = $app->request()->post('interests');
    $work = $app->request()->post('work');
    $location = $app->request()->post('location');
    $age=$app->request()->post('age');

    if (strlen($about) < 2 || strlen($about) > 512) {
        array_push($errorList, "About info should be 2-512 characters long ");
    }
    if (strlen($lookingfor) < 2 || strlen($lookingfor) > 50) {
        array_push($errorList, "Looking for info should be 2-50 characters long ");
    }

    if (!is_numeric($ageFrom) || $ageFrom < 18 || $ageTo > 100) {
        array_push($errorList, "Age start range should be a number between 18-100 ");
    }
    if (!is_numeric($ageTo) || $ageTo < 18 || $ageTo > 100 || $ageFrom > $ageTo) {
        array_push($errorList, "Age uppper range should be a number between 18-100 and smaller that starting age ");
    }
    if (strlen($education) < 2 || strlen($education) > 50) {
        array_push($errorList, "Education info should be 2-50 characters long ");
    }
    if (strlen($interests) < 2 || strlen($interests) > 50) {
        array_push($errorList, "Interests info should be 2-50 characters long ");
    }
    if (strlen($work) < 2 || strlen($work) > 45) {
        array_push($errorList, "Work info should be 2-45 characters long ");
    }



    $image = $_FILES['image'];
    if ($image) {
        $imageInfo = getimagesize($image['tmp_name']);
        if (!$imageInfo) {
            array_push($errorList, "File does not look like a valid image");
        } else {
            // never allow '..' in the file name
            if (strstr($image['name'], '..')) {
                array_push($errorList, "File name invalid");
            }
            // only allow select extensions
            $ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));
            if (!in_array($ext, array('jpg', 'jpeg', 'gif', 'png'))) {
                array_push($errorList, "File extension invalid");
            }
            // do not allow to override an existing file
            if (file_exists('uploads/' . $image['name'])) {
                array_push($errorList, "File name already exists, refusing to override.");
            }
        }
    } else {
        array_push($errorList, "No image received! ");
    }
    //

    $valueList = array('BLAboutMe' => $about, 'BLLookingfor' => $lookingfor, 'BLAgeRangeFrom' => $ageFrom, 'BLAgeRangeTo' => $ageTo,
        'BLEducation' => $education, 'BLInterests' => $interests, 'BLWork' => $work,'BLAge'=>$age,'BLLocation'=>$location,'BLLookingFor'=>$lookingfor,'BLPhoto'=>$image['name']);
    if ($errorList) {
        $app->render('profileedit.html.twig', array(
            'u' => $valueList, 'errorList' => $errorList));
    } else {
        move_uploaded_file($image['tmp_name'], 'uploads/' . $image['name']);
        DB::update('blazeaccounts', array(
            'BLPhoto' => $image['name'],'BLAboutMe' => $about, 'BLLookingfor' => $lookingfor, 'BLAgeRangeFrom' => $ageFrom, 'BLAgeRangeTo' => $ageTo,
        'BLEducation' => $education, 'BLInterests' => $interests, 'BLWork' => $work,'BLAge'=>$age,'BLLocation'=>$location,'BLLookingFor'=>$lookingfor
                ), "BLUserId=%s", $id);
        $current_user = DB::queryFirstRow("SELECT * FROM blazeaccounts WHERE BLUserId=%s", $id);

        $app->render("profile_view.html.twig",array('u'=>$current_user));
    }
});

$app->get('/profile(/:id)', function($otherUserId = null) use ($app) {
    if(!isset($_SESSION['id'])){
        $app->render("unauthorised.html.twig");
        return;
    }
    if ($otherUserId) {
        $id = $otherUserId;
    } else {
        $id = $_SESSION['id'];
        $isOwnProfile = true;
    }
    $user = DB::queryFirstRow("SELECT * FROM blazeaccounts WHERE BLUserId=%s", $id);
    $name = $user['BLFirstName'];
    $app->render("profile_view.html.twig", array('u' => $user, 'isOwn' => $isOwnProfile));
});

$app->get('/logout', function($otherUserId = null) use ($app) {
    unset($_SESSION['fb_id']);
    unset($_SESSION['id']);
    unset($_SESSION['name']);
    $app->response->redirect($app->urlFor('main'), 303);
});
 // AJAX call
$app->post('/like', function() use ($app,$log) {
    if(!isset($_SESSION['id'])){
        $app->render("unauthorised.html.twig");
        return;
    }
    $json=$app->request()->getBody();
    $id= json_decode($json,true); // second param forces it to return an array
    //FIX ME : validate todo
    $clickedUserId=$id['userId'];
    $userId=$_SESSION['id'];
    $likeRecord=array('BLUserId'=>$userId,'LLikedUserId'=>$clickedUserId);
      
        DB:: insert('likes',$likeRecord);
    $app->response()->status(201);
    $matchOrNo=DB:: query("SELECT * from likes WHERE BLUserID=%s and LLikedUserID=%s",$clickedUserId,$userId);
    if($matchOrNo){
        DB:: insert('conversations',array('CAuthorID'=>$userId,'CReceiver'=>$clickedUserId));
        $insertedConvo=DB::insertId();
        DB::insert('messages',array('ConvoID'=>$insertedConvo,'MsgContent'=>"Looks like there is a match!"));
    }
    else echo json_encode("no macth");
    
    
});
$app->get('/getStatus', function() use ($app) {
//State 1: First Show
    $areNewMessages=DB:: query("SELECT * from messages as m INNER JOIN conversations as c on m.ConvoID=c.ConvoID 
         where m.wasSeen='false' AND (c.CAuthorID=%s OR c.CReceiver=%s)",$_SESSION['id'],$_SESSION['id']);
    
    if (!$areNewMessages){
        echo json_encode('false');
    }
    else echo json_encode('true');
    });

//END OF VICTORIA'S TERRITORY *****************************8
//ROB'S TERRITOTY

$app->get('/home', function() use ($app) {
    if(!isset($_SESSION['id'])){
        $app->render("unauthorised.html.twig");
        return;
    }
//State 1: First Show
    $closeArea = DB::queryFirstRow("SELECT BLLocation, BLGender FROM blazeaccounts WHERE BLUserID =%s", $_SESSION['id']);
    $matchList = DB::query("SELECT * FROM blazeaccounts as a WHERE a.BLUserID !=%s AND a.BLGender !=%s"
            , $_SESSION['id'], $closeArea['BLGender']);
    $app->render('home.html.twig', array('al' => $matchList));
})->name("home");


$app->get('/matches', function() use ($app) {
    if(!isset($_SESSION['id'])){
        $app->render("unauthorised.html.twig");
        return;
    }
//State 1: First Show
    $closeArea = DB::queryFirstRow("SELECT BLLocation, BLGender, BLAgeRangeFrom, BLAgeRangeTo FROM blazeaccounts WHERE BLUserID =%s", $_SESSION['id']);
    $matchList = DB::query("SELECT * FROM blazeaccounts WHERE BLLocation = %s AND BLUserID !=%s AND BLGender !=%s AND BLAge >=%s AND BLAge <= %s", $closeArea['BLLocation'], $_SESSION['id'], $closeArea['BLGender'], $closeArea['BLAgeRangeFrom'], $closeArea['BLAgeRangeTo']);
    $app->render('mymatches.html.twig', array('al' => $matchList));
})->name("matches");

$app->get('/inbox', function() use ($app) {
    if(!isset($_SESSION['id'])){
        $app->render("unauthorised.html.twig");
        return;
    }
//State 1: First Show
    $myId=$_SESSION['id'];
    $messageList = DB::query("SELECT x.ConvoID, x.CAuthorID, x.CReceiver,v.MsgTime,v.MsgContent,v.wasSeen, b.BLPhoto AS SenderPhoto, b.BLFirstName AS SenderFN, n.BLPhoto AS ReceiverPhoto, n.BLFirstName AS ReceiverFN 
FROM conversations as x 
JOIN messages as v on x.ConvoID = v.ConvoID 
JOIN blazeaccounts as b on x.CReceiver = b.BLUserID
JOIN blazeaccounts as n on x.CAuthorID = n.BLUserID group by x.ConvoID
having x.CAuthorID= %s OR x.CReceiver=%s" ,$_SESSION['id'],$_SESSION['id']);
//    $messageList=array();
//    $i=0;
//    $convoList=DB::query('SELECT * FROM conversations WHERE CAuthorId=%s OR CReceiver=%s ',$myId,$myId);
//    foreach($convoList as $convo){
//        $convoID=$convo['ConvoID'];
//        
//        
//    }
   
    
    $app->render('inbox.html.twig', array('al' => $messageList,'user'=> $_SESSION['id']));
})->name("inbox");

$app->get('/inbox/:id', function($id) use ($app) {
    if(!isset($_SESSION['id'])){
        $app->render("unauthorised.html.twig");
        return;
    }
//State 1: First Show
    $messageList = DB::query("SELECT z.ConvoID, z.MsgContent, z.MsgTime, x.CAuthorID, x.CReceiver FROM messages as z JOIN conversations as x ON z.ConvoID=x.ConvoID WHERE z.ConvoID =%s",$id);
    DB::update('messages',array('wasSeen'=>'true'),"ConvoID=%s",$id);
    $userList = DB::query("SELECT BLUserID, BLFirstName FROM blazeaccounts");
    $app->render('convo.html.twig', array('al' => $messageList,'user'=> $_SESSION['id'], 'userList'=>$userList));
})->name("inbox/:id");

$app->post('/inbox/sendMsg', function() use ($app) {
    if(!isset($_SESSION['id'])){
        $app->render("unauthorised.html.twig");
        return;
    }
$json=$app->request()->getBody();
    $msg= json_decode($json,true); // second param forces it to return an array
    DB::insert('messages',$msg);
    echo json_encode(DB::insertId());
});

$app->run();
